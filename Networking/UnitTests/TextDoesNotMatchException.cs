﻿// -----------------------------------------------------------------------
// <author> 
//      Jude K Anil
// </author>
//
// <date> 
//      03-11-2018 
// </date>
// 
// <reviewer>
//      Libin N George
// </reviewer>
//
// <copyright file="TextDoesNotMatchException.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary> 
//      This file is a part of Networking Module Unit Testing.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An exception class which corresponds to the case where two
    /// strings, that were supposed to be equal, are not equal.
    /// </summary>
    public class TextDoesNotMatchException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextDoesNotMatchException" /> class
        /// </summary>
        public TextDoesNotMatchException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextDoesNotMatchException" /> class
        /// </summary>
        /// <param name="message">Stores a string related to the raising of the exception.</param>
        public TextDoesNotMatchException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextDoesNotMatchException" /> class
        /// </summary>
        /// <param name="message">Stores a string related to the raising of the exception.</param>
        /// <param name="inner">Stores a exception that is related to the raising of the exception.</param>
        public TextDoesNotMatchException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextDoesNotMatchException" /> class
        /// </summary>
        /// <param name="info">Stores the info.</param>
        /// <param name="context">Stores the context.</param>
        protected TextDoesNotMatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
