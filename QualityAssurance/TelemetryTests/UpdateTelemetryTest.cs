﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     16th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="UpdateTelemetryTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class tests whether updation of telemetry with
//    the TelemetryCollector works correctly.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to test retrieval and register functionalities of telemetry.
    /// </summary>
    public class UpdateTelemetryTest : ITest
    {
        /// <summary>
        /// Logger instance used to log messages while testing.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateTelemetryTest" /> class.
        /// </summary>
        /// <param name="logger">The logger instance to be used to log messages while testing.</param>
        public UpdateTelemetryTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Runs tests to check update functionality of telemetry.
        /// </summary>
        /// <returns>Success status of tests run.</returns>
        public bool Run()
        {
            ITelemetryCollector telemetryCollector = TelemetryCollector.Instance;
            var sampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("SampleTelemetry");

            if (sampleTelemetry == null)
            {
                telemetryCollector.RegisterTelemetry("SampleTelemetry", new SampleTelemetry());
                sampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("SampleTelemetry");
            }
                        
            // Check if updates are retained correctly.
            this.logger.LogInfo("Updating and retrieving telemetry...");

            var updatedCorrectly = false;
            var currentQuantity = int.Parse(sampleTelemetry.DataCapture["Quantity"], CultureInfo.InvariantCulture);
            
            // Update value.
            sampleTelemetry.IncrementQuantity();

            var retrievedSampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("SampleTelemetry");
            var retrievedQuantity = int.Parse(retrievedSampleTelemetry.DataCapture["Quantity"], CultureInfo.InvariantCulture);

            // Check if retrieved value is correct.
            if (retrievedQuantity == currentQuantity + 1)
            {
                this.logger.LogSuccess("Updating and retrieval happened correctly. Functionality is working as intended.");
                updatedCorrectly = true;
            }
            else
            {
                this.logger.LogWarning("Updating did not happen correctly. Functionality not working as intended.");
            }

            return updatedCorrectly;
        }
    }
}
