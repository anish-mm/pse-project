﻿//-----------------------------------------------------------------------
// <author>
//      Suman Saurav Panda
// </author>
// <reviewer>
//      Ravindra Kumar, Axel James
// </reviewer>
// <date>
//      28-Oct-2018
// </date>
// <summary>
//      Server side receive image class implementation 
// </summary>
// <copyright file="ReceiveImage.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using Masti.QualityAssurance;
    using Masti.Schema;
    using Networking;
    
    /// <summary>
    /// This class creates instance which takes rawdata given by the networking module. This class is the publisher for
    /// the image to the UI module. It checks the integrity received data and then either publishes to UI module upon succes
    /// otherwise notify error to its upper module.
    /// </summary>
    public class ReceiveImage
    {
        /// <summary>
        /// lock to make factory thread-safe
        /// </summary>
        private readonly object padlock = new object();

        /// <summary>
        /// Refers a timer for repeated capturing and sending of captured screen.
        /// </summary>
        private Timer imageTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveImage"/> class. 
        /// </summary>
        /// <param name="schema">takes the object schema to unpack the rawstring received from networking</param>
        /// <param name="compression">compression takes the compression object to use decompress to get back the pixeliamge</param>
        /// <param name="imageCommunication">this is the signalling class object of image module which would deliver
        /// the raw string from network</param>
        public ReceiveImage(ISchema schema, ICompression compression, ImageCommunication imageCommunication)
        {
            this.Start = false;
            this.Schema = schema;
            this.Compression = compression;
            this.ImageCommunication = imageCommunication;
            this.ImageCommunication.SubscribeForSignalReceival(Signal.Image, this.OnRecievingimage);
            this.Error = new ErrorEventArgs();
        }

        /// <summary>
        /// event to publish the  Image decoded to its subscriber
        /// </summary>
        private event EventHandler<ImageEventArgs> ImageDecoded = null;

        /// <summary>
        /// Event to publish Error on image decoding to its subscriber
        /// </summary>
        private event EventHandler<ErrorEventArgs> ErrorImageDecoded = null;

        /// <summary>
        /// Gets or sets the string received from ImageCommunication
        /// </summary>
        public string RawString { get; set; }

        /// <summary>
        /// Gets or sets ip address of client
        /// </summary>
        public IPAddress ClientIP { get; set; }

        /// <summary>
        /// Gets or sets error object which has error message in it
        /// </summary>
        public ErrorEventArgs Error { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to receive image object is locked or not
        /// Once it sends the stop signal it would stop accepting accepting image for that session
        /// </summary>
        public bool Start { get; set; }

        /// <summary>
        /// Gets or sets the image communication object
        /// </summary>
        private ImageCommunication ImageCommunication { get; set; }

        /// <summary>
        /// Gets or sets schema obj for unpacking method; 
        /// </summary>
        private ISchema Schema { get; set; }

        /// <summary>
        /// Gets or sets the compression obj for decompression method;
        /// </summary>
        private ICompression Compression { get; set; }

        /// <summary>
        /// Gets or sets imageBitMap which would be given to UI 
        /// </summary>
        private Bitmap ImageBitMap { get; set; }

        /// <summary>
        /// This function resets the timer objects elapsed time on getting a correct image
        /// </summary>
        public void ResetTimer()
        {
            this.imageTimer.Stop();
            this.imageTimer.Start();
            MastiDiagnostics.LogInfo("Reset timer");
        }

        /// <summary>
        /// this method destroys the image timer which created at the starting of the image sharing session
        /// </summary>
        public void DestroyTimerObj()
        {
            MastiDiagnostics.LogInfo("Timer destroyed");
            this.imageTimer.Close();
        }

        /// <summary>
        /// method is used to register event handler by the subscriber for the iamgeReceived event
        /// </summary>
        /// <param name="onImageDecoded">delegate function which would be given by the subscriber(UI module)
        /// for getting the image from ImageProcessing module</param>
        public void RegisterListenerImageDecoded(EventHandler<ImageEventArgs> onImageDecoded)
        {
            this.ImageDecoded += onImageDecoded;
            return;
        }

        /// <summary>
        /// method is used to register event handler for the error notification
        /// </summary>
        /// <param name="onErrorImageDecoded">delegate function for the event</param>
        public void RegisterListenerErrorImageDecoded(EventHandler<ErrorEventArgs> onErrorImageDecoded)
        {
            this.ErrorImageDecoded += onErrorImageDecoded;
            return;
        }

        /// <summary>
        /// this is event handler to signaling class Upon getting data from networking module.
        /// After parsing the rawData if some error found it sets the ErrorFlag of the object 
        /// so that the error can be propagated to the previous modules.
        /// </summary>
        /// <param name="rawData">data from networking module which would then be processed to send to the UI module</param>
        /// <param name="ip">ip address of the client required by the UI team</param>
        public void OnRecievingimage(string rawData, IPAddress ip)
        {
            lock (this.padlock)
            {
                MastiDiagnostics.LogInfo("Image communication:: invoked the On receive image handler");
                if (!this.Start)
                {
                    MastiDiagnostics.LogInfo("won't accept data anymore as stop signal is sent to client");
                    return;
                }

                this.RawString = rawData;
                this.ClientIP = ip;
                if (!this.GetBitMap())
                {
                    this.Error.StopSignalOnError = false;
                    MastiDiagnostics.LogError(this.Error.ErrorMessage);
                    this.ImageCommunication.SignalImageModule(this.ClientIP, Signal.Resend);
                    MastiDiagnostics.LogInfo("requested resend to client");
                    this.OnErrorImageDecoded();
                }
                else
                {
                    this.ResetTimer();
                    this.OnImageDecoded();
                    MastiDiagnostics.LogInfo("Passed the image to UI successfully");
                }
            }

            return;
        }

        /// <summary>
        /// It converts the rawstring received form networking to actual bitmap of the image
        /// </summary>
        /// <returns>true if it successfully send the image to UI module
        /// otherwise if some parsing error happens while unpacking using shema method and decompression
        /// it returns false.</returns>
        public bool GetBitMap()
        {
            Dictionary<int, Bitmap> imageMap;
            IDictionary<string, string> unpackedDicitionary;

            try
            {
                unpackedDicitionary = this.Schema.Decode(this.RawString, false);
            }
            catch (Exception)
            {
                this.Error.ErrorMessage = "Schema unable to decode";
                return false;
            }

            MastiDiagnostics.LogInfo("successfully unpacked by schema");

            if (!(unpackedDicitionary.ContainsKey("implementDiff") && unpackedDicitionary.ContainsKey("imageDictionary")))
            {
                this.Error.ErrorMessage = "unexpected format received";
                return false;
            }

            MastiDiagnostics.LogInfo("dictionary key value matched");

            bool diffImplementationFlag = string.Equals(unpackedDicitionary["implementDiff"], "true", StringComparison.Ordinal);
            string serializedImageDictionary = unpackedDicitionary["imageDictionary"];

            try
            {
                imageMap = this.Compression.StringToBmpDict(serializedImageDictionary);
                this.ImageBitMap = this.Compression.Decompress(imageMap, diffImplementationFlag);
            }
            catch (Exception)
            {
                this.Error.ErrorMessage = "Image decompression returned exception";
                return false;
            }

            if (this.ImageBitMap == null)
            {
                this.Error.ErrorMessage = "image bit map stitching unsuccessful";
                return false;
            }

            MastiDiagnostics.LogInfo("image decoded successfully");
            return true;
        }

        /// <summary>
        /// this is a method to create a Image timer object.
        /// this timer would be calling elapsed event after 10 second of not receiving any valid image from the client
        /// </summary>
        public void CreateTimerObj()
        {
            this.imageTimer = new Timer();
            this.imageTimer.Elapsed += this.Timer_Elapsed;
            this.imageTimer.Interval = 30000;
            this.imageTimer.AutoReset = false;
            this.imageTimer.Enabled = true;
            MastiDiagnostics.LogInfo("Timer started");
        }

        /// <summary>
        /// raises the event for error
        /// </summary>
        protected virtual void OnErrorImageDecoded()
        {
            if (this.Error.StopSignalOnError)
            {
                MastiDiagnostics.LogInfo("asked the client to stop image sharing");
                this.ImageCommunication.SignalImageModule(this.ClientIP, Signal.Stop);
                this.DestroyTimerObj();
            }

            this.Error.ClientIP = this.ClientIP.ToString();
            this.ErrorImageDecoded?.Invoke(this, this.Error);
            MastiDiagnostics.LogInfo("Error message passed to UI");
        }

        /// <summary>
        /// raises the event to pass the encoded image to its subscriber
        /// </summary>
        protected virtual void OnImageDecoded()
        {
            ImageEventArgs imageEventArgs = new ImageEventArgs() { ImageBitmap = this.ImageBitMap, ClientIP = this.ClientIP.ToString() };
            this.ImageDecoded?.Invoke(this, imageEventArgs);
        }

        /// <summary>
        /// this event handler is called after a given time period is elapsed 
        /// </summary>
        /// <param name="sender">default sender parameter for system timer event handler</param>
        /// <param name="e">default elapsed event arguement for system timer</param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (this.padlock)
            {
                this.Start = false;
            }

            this.Error.StopSignalOnError = true;
            this.OnErrorImageDecoded();
        }
    }
}
