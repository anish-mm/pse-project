//-----------------------------------------------------------------------
// <author> 
//     Rahul Dhawan
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="Messager.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our messaging model.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Networking;
    using Masti.Persistence;
    using Masti.QualityAssurance;
    using Masti.Schema;
    using static Masti.Messenger.Handler;

    /// <summary>
    /// This is our main class implementing our interface IUXMessage
    /// </summary>
    public class Messager : IUXMessage
    {
        /// <summary>
        /// Test variables for unit testing
        /// </summary>
        public int TestStatusVariable = 0;

        /// <summary>
        /// Test variables for unit testing
        /// </summary>
        public int TestDataVariable = 0;

        /// <summary>
        /// This is an object instance of schema
        /// </summary>
        public ISchema SchemaObj;

        /// <summary>
        /// This is an object instance of persistence
        /// </summary>
        public IPersistence PersistObj;

        /// <summary>
        /// This is an object instance of communication
        /// </summary>
        public ICommunication Comm;

        /// <summary>
        /// user name 
        /// </summary>
        private readonly string uname;

        /// <summary>
        /// This is handler to handle the callbacks of UX model when data is received
        /// </summary>
        private Handler.DataReceiverHandler dataReceiver;

        /// <summary>
        /// This is handler to handle the callbacks of UX model when status of data is received
        /// </summary>
        private Handler.DataStatusHandlers statusReciever;

        /// <summary>
        /// This is for handling callbacks used while making initial connection
        /// </summary>
        private Handler.ConnectHandlers connectifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="Messager"/> class
        /// </summary>
        /// <param name="port">Port number</param>
        public Messager(int port)
        {
            ITelemetryCollector messagingTelemetryCollector = TelemetryCollector.Instance;

            // Register telemetry.
            messagingTelemetryCollector.RegisterTelemetry("MessagingTelemetry", new MessagingTelemetry());
            this.uname = "professor";
            this.SchemaObj = new MessageSchema();
            this.PersistObj = new Persistence();
            this.Comm = CommunicationFactory.GetCommunicator(port);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Messager"/> class
        /// </summary>
        /// <param name="ip">IP address</param>
        /// <param name="port">Port number</param>
        /// <param name="uname">User name</param>
        public Messager(string ip, int port, string uname)
        {
            ITelemetryCollector messagingTelemetryCollector = TelemetryCollector.Instance;

            // Register telemetry.
            messagingTelemetryCollector.RegisterTelemetry("MessagingTelemetry", new MessagingTelemetry());
            this.SchemaObj = new MessageSchema();
            this.PersistObj = new Persistence();
            this.Comm = CommunicationFactory.GetCommunicator(ip, port);
            this.uname = uname;
        }

        /// <summary>
        /// This is a callback function subscribed to communication team to receive the message
        /// </summary>
        /// <param name="message">Message received</param>
        /// <param name="ipaddres">IP address</param>
        public void DataCallback(string message, IPAddress ipaddres)
        {
            try
            {
                IDictionary<string, string> decodedMsg;
                decodedMsg = this.SchemaObj.Decode(message, false);
                if (ipaddres == null)
                {
                    throw new ArgumentNullException("ipaddres");
                }

                string fromIP = ipaddres.ToString();
                string pureMessage = decodedMsg["Msg"];
                string toIP = decodedMsg["toIP"];
                string dateTime = decodedMsg["dateTime"];
                string flag = decodedMsg["flag"];

                if (string.Equals("True", flag))
                {
                    this.dataReceiver(pureMessage, toIP, fromIP, dateTime);
                    this.TestDataVariable = 1;
                    ITelemetryCollector messagingTelemetryCollector = TelemetryCollector.Instance;

                    // Extract your telemetry data from the collector.
                    MessagingTelemetry messagingTelemetry = (MessagingTelemetry)messagingTelemetryCollector.GetTelemetryObject("MessagingTelemetry");
                    messagingTelemetry.CalculateRec();
                }
                else
                {
                    this.connectifier(fromIP, pureMessage);
                    this.TestDataVariable = 2;
                    ITelemetryCollector messagingTelemetryCollector = TelemetryCollector.Instance;

                    // Extract your telemetry data from the collector.
                    MessagingTelemetry messagingTelemetry = (MessagingTelemetry)messagingTelemetryCollector.GetTelemetryObject("MessagingTelemetry");
                    messagingTelemetry.CalculateConnection();
                }

                MastiDiagnostics.LogSuccess(string.Format(CultureInfo.CurrentCulture, "data sent back successfully"));
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to send data. {0}", e.Message));
                Console.WriteLine("Failed to send data!!");
            }
        }

        /// <summary>
        /// This is a callback function subscribed to communication team to receive the status
        /// </summary>
        /// <param name="message">Message sent</param>
        /// <param name="statuscode">a type of enum, success or failure</param>
        public void StatusCallback(string message, Masti.Networking.StatusCode statuscode)
        {
            try
            {
                IDictionary<string, string> decodedMsg;
                decodedMsg = this.SchemaObj.Decode(message, false);
                string pureMessage = decodedMsg["Msg"];
                string fromIP = decodedMsg["toIP"];
                Handler.StatusCode newStatus = (Handler.StatusCode)statuscode;
                if (decodedMsg["flag"] == "False" && newStatus == Handler.StatusCode.Success)
                {
                    newStatus = Handler.StatusCode.ConnectionEstablished;
                    this.TestStatusVariable = 1;
                }
                else if (decodedMsg["flag"] == "False" && newStatus == Handler.StatusCode.Failure)
                {
                    newStatus = Handler.StatusCode.ConnectionError;
                    this.TestStatusVariable = 2;
                }
                else if (decodedMsg["flag"] == "True" && newStatus == Handler.StatusCode.Failure)
                {
                    this.TestStatusVariable = 3;
                }
                else
                {
                    this.TestStatusVariable = 4;
                }

                this.statusReciever(newStatus, fromIP, pureMessage);
                MastiDiagnostics.LogSuccess(string.Format(CultureInfo.CurrentCulture, "status data sent back successfully"));
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to send status of data. {0}", e.Message));
            }
        }

        /// <summary>
        /// this is to send message
        /// </summary>
        /// <param name="message">Message for sending</param>
        /// <param name="toIP">IP address</param>
        /// <param name="dateTime">Time stamp</param>
        /// <returns>success or failure</returns>
        public bool SendMessage(string message, string toIP, string dateTime)
        {
            try
            {
                bool status;
                string encodeMsg;
                string fromIP = this.Comm.LocalIP;

                IPAddress targetAddress = IPAddress.Parse(toIP);
                Dictionary<string, string> msg = new Dictionary<string, string>
                {
                    ["Msg"] = message,
                    ["fromIP"] = fromIP,
                    ["toIP"] = toIP,
                    ["flag"] = "True",
                    ["dateTime"] = dateTime
                };
                msg["uname"] = this.uname;
                encodeMsg = this.SchemaObj.Encode(msg);
                MastiDiagnostics.LogSuccess(string.Format(CultureInfo.CurrentCulture, "successfully sent message"));

                status = this.Comm.Send(encodeMsg, targetAddress, DataType.Message);
                if (status)
                {
                    ITelemetryCollector messagingTelemetryCollector = TelemetryCollector.Instance;

                    // Extract your telemetry data from the collector.
                    MessagingTelemetry messagingTelemetry = (MessagingTelemetry)messagingTelemetryCollector.GetTelemetryObject("MessagingTelemetry");
                    messagingTelemetry.CalculateSend();
                }

                return status;
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to send message to networking module. {0}", e.Message));
                Console.WriteLine("Failed to send message to networking module!!");
                return false;
            }
        }

        /// <summary>
        /// This function is to subscribe callbacks of UX model for receiving data
        /// </summary>
        /// <param name="handler">Callback functions</param>
        /// <returns>Return the sucess or failure of subscribing</returns>
        public bool SubscribeToDataReceiver(DataReceiverHandler handler)
        {
            try
            {
                this.dataReceiver = new Handler.DataReceiverHandler(handler);
                MastiDiagnostics.LogSuccess(string.Format(CultureInfo.CurrentCulture, "successfully subscription for data receiver"));
                return this.Comm.SubscribeForDataReceival(DataType.Message, this.DataCallback);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to subscribe for data receiver. {0}", e.Message));
                Console.WriteLine("Failed to subscribe for data receiver.");
                return false;
            }
        }

        /// <summary>
        /// This function is to subscribe callbacks of UX model for receiving status of sent data
        /// </summary>
        /// <param name="handler">callback function of UX</param>
        /// <returns>Return the success or failure of sunscribing</returns>
        public bool SubscribeToStatusReceiver(DataStatusHandlers handler)
        {
            try
            {
                this.statusReciever = new Handler.DataStatusHandlers(handler);
                return this.Comm.SubscribeForDataStatus(DataType.Message, this.StatusCallback);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to subscribe for status receiver. {0}", e.Message));
                Console.WriteLine("Failed to subscribe for status receiver.");
                return false;
            }
        }

        /// <summary>
        /// This function is to subscribe callbacks of UX model for starting connection by sending the first message
        /// </summary>
        /// <param name="handler">call back function</param>
        /// <returns>Return the success of the subscribing</returns>
        public bool SubscribeToConnectifier(ConnectHandlers handler)
        {
            try
            {
                this.connectifier = new Handler.ConnectHandlers(handler);
                return true;
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to subscribe for connectifier. {0}", e.Message));
                Console.WriteLine("Failed to subscribe for Connectifier .");
                return false;
            }
        }

        /// <summary>
        /// This function is for retrieving message from the persistance
        /// </summary>
        /// <param name="startSession">start session</param>
        /// <param name="endSession">end session</param>
        /// <returns>path of the file in which messages are stored</returns>
        public string RetrieveMessage(int startSession, int endSession)
        {
            try
            {
                string path = "log.txt";
                var retrivingMessage = new Collection<string>(this.PersistObj.RetrieveSession(startSession, endSession));
                string concatMessage = string.Empty;
                concatMessage = string.Join(" ", retrivingMessage.ToArray());
                System.IO.File.WriteAllText(path, concatMessage);
                if (string.IsNullOrEmpty(concatMessage))
                {
                    return string.Empty;
                }

                return path;
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to Reterive message. {0}", e.Message));
                Console.WriteLine("Failed to Retrieve message.");
                return "false";
            }
        }

        /// <summary>
        /// This function is for deleting the messages
        /// </summary>
        /// <param name="startSession">start session</param>
        /// <param name="endSession">end session</param>
        /// <returns>Return the success or failure of deleting the messages</returns>
        public int DeleteMessage(int startSession, int endSession)
        {
            try
            {
                return this.PersistObj.DeleteSession(startSession, endSession);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to Delete message. {0}", e.Message));
                Console.WriteLine("Failed to Delete message.");
                return -1;
            }
        }

        /// <summary>
        /// This function is for storing the messages
        /// </summary>
        /// <param name="currSession">current session</param>
        /// <returns>Return the success or failure of storing the messages</returns>
        public bool StoreMessage(Dictionary<string, string> currSession)
        {
            try
            {
                string concatMessage = string.Empty;
                if (currSession == null)
                {
                    throw new ArgumentNullException("currSession");
                }

                foreach (KeyValuePair<string, string> messageKeyValuePair in currSession)
                {
                    concatMessage += messageKeyValuePair.Value + " ";
                }

                return this.PersistObj.SaveSession(concatMessage);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to store message. {0}", e.Message));
                Console.WriteLine("Failed to store message.");
                return false;
            }
        }

        /// <summary>
        /// This function is used to establish the connection by sending the first message
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="toIP">target IP</param>
        /// <returns>Return the success or failure of connection establishes</returns>
        public bool Connectify(string userName, string toIP)
        {
            try
            {
                string encodeMsg;
                string fromIP = this.Comm.LocalIP;

                IPAddress targetAddress = IPAddress.Parse(toIP);
                Dictionary<string, string> msg = new Dictionary<string, string>
                {
                    ["Msg"] = userName,
                    ["fromIP"] = fromIP,
                    ["toIP"] = toIP,
                    ["flag"] = "False"
                };

                var time = DateTime.Now;
                string formattedTime = time.ToString("yyyy, MM, dd, hh, mm, ss", CultureInfo.InvariantCulture);
                msg["dateTime"] = formattedTime;
                encodeMsg = this.SchemaObj.Encode(msg);
                return this.Comm.Send(encodeMsg, targetAddress, DataType.Message);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to establish connection. {0}", e.Message));
                Console.WriteLine("Failed to establish connection.");
                return false;
            }
        }

        /// <summary>
        /// Returns the current session ID.
        /// </summary>
        /// <returns>Current session ID</returns>
        public int GetCurrentSessionId()
        {
            try
            {
                return this.PersistObj.GetCurrentSessionId();
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to fetch SessionId. {0}", e.Message));
                Console.WriteLine("Failed to fetch SessionId.");
                return 0;
            }
        }
    }
}